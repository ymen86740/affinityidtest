import React,{useEffect} from 'react';
import styles from './EmployeeManagementPanel.module.css';
import Container from '../../Container';
import {space,locale} from '../../DatabaseInterface';
import {ReactComponent as Logo} from './affinityid-logo.svg';
import SearchInput from './subcomponents/SearchInput/SearchInput';
import TitlePanel from './subcomponents/TitlePanel/TitlePanel';
import EmployeeList from './subcomponents/EmployeeList/EmployeeList';
import EmployeeDetail from './subcomponents/EmployeeCRUD/EmployeeDetail';
import EmployeeUpdate from './subcomponents/EmployeeCRUD/EmployeeUpdate';
import EmployeeCreate from './subcomponents/EmployeeCRUD/EmployeeCreate';
import ScrolldownPanel from './subcomponents/ScrolldownPanel/ScrolldownPanel';
import {Button,message} from 'antd';

export default ()=>{

    const state=Container.useContainer();

    // pull all the employee data from contentful, if need more fields, setup the fields on contentful
    useEffect(()=>{
        let data=[];
        space.then(space=>space.getEntries())
        .then(response=>{
            response.items.map(x=>{
                return space.then(space=>space.getAsset(x.fields.profileImage?x.fields.profileImage[locale].sys.id:''))
                    .then(asset=>{
                        data.push({
                            id:x.sys.id,
                            profileImageUrl:asset.fields.file[locale].url,
                            name:x.fields.name[locale],
                            email:x.fields.email[locale],
                            role:x.fields.role[locale],
                            team:x.fields.team[locale],
                            address:x.fields.address?x.fields.address[locale]:'',
                            city:x.fields.city?x.fields.city[locale]:'',
                            createdDate:x.sys.createdAt
                            // if need more fields, add them below
                        });
                    })
                    .catch(console.error);
            })
        });
        
        message.loading("Loading...",3,()=>{
            if(localStorage.getItem('selectedEmployeeId')!==null){
                state.setEmployeeDetail(data.filter(x=>{
                    return x.id===localStorage.getItem('selectedEmployeeId');
                })[0]);
                state.setShowEmployeeDetail(true);
            }
            state.setShowEmployeeCreate(false);
            state.setShowConfirmPanel(false);
            state.setShowEmployeeUpdate(false);
            setTimeout(()=>{
                state.setEmployees(data);
            },300);
        });
    },[state.refresh]);
    
    return(
        <div className={styles.Container}>
            <div className={styles.EmployeeListPanel}>
                <Logo className={styles.Logo}></Logo>
                <SearchInput></SearchInput>
                <TitlePanel title="Admin" sort={true}></TitlePanel>
                <EmployeeList></EmployeeList>
                <ScrolldownPanel></ScrolldownPanel>
            </div>
            <div className={styles.EmployeeCRUDPanel}>
                <Button style={{width:'100%',backgroundColor:'#000000',color:'#ffffff',borderColor:'#000000'}} size="large" onClick={()=>{
                                state.setShowEmployeeDetail(false);
                                state.setShowEmployeeUpdate(false);
                                setTimeout(()=>{
                                    state.setShowEmployeeCreate(true);
                                },300);
                }}>CREATE A NEW EMPLOYEE</Button>
                <EmployeeDetail></EmployeeDetail>
                <EmployeeUpdate></EmployeeUpdate>
                <EmployeeCreate></EmployeeCreate>
            </div>
        </div>
    );
}