import React from 'react';
import styles from './ScrolldownPanel.module.css';
import {ReactComponent as Scrolldown} from './angle-double-down.svg';
import Container from '../../../../Container';

export default ()=>{

    const state=Container.useContainer();

    return(
        <div className={styles.Container}>
            {
                state.searchedEmployees.length>6?
                <Scrolldown className={styles.Icon} onClick={()=>{
                    
                }}></Scrolldown>
                :<div></div>
            }
        </div>
    );
}