import React,{useEffect} from 'react';
import styles from './EmployeeList.module.css';
import EmployeeListItem from './EmployeeListItem';
import Container from '../../../../Container';

export default ()=>{

    const state=Container.useContainer();

    useEffect(()=>{
        state.setSearchedEmployees(state.employees);
    },[state.employees]);

    return(
        <div className={styles.Container}>
            {
                state.searchedEmployees.map(x=>{
                    return (
                        x.role==="Admin"?<EmployeeListItem key={x.id} detail={x}></EmployeeListItem>:null
                    )
                })
            }
            <div className={styles.Title}>Employee</div>
            {
                state.searchedEmployees.map(x=>{
                    return (
                        x.role==="Employee"?<EmployeeListItem key={x.id} detail={x}></EmployeeListItem>:null
                    )
                })
            }
        </div>
    );
}