import React from 'react';
import styles from './EmployeeListItem.module.css';
import Container from '../../../../Container';

export default (props)=>{

    const state=Container.useContainer();

    return(
        <div key={props.detail.id} className={styles.Container} onClick={()=>{
            state.setShowEmployeeDetail(false);
            state.setShowEmployeeUpdate(false);
            state.setShowEmployeeCreate(false);
            setTimeout(()=>{
                state.setShowEmployeeDetail(true);
            },300);
            setTimeout(()=>{
                state.setEmployeeDetail(props.detail);
            },300);
        }}>
            <img src={props.detail.profileImageUrl} width="60" height="60" alt={props.detail.name}></img>
            <div>
                <div className={styles.Name}>{props.detail.name}</div>
                <div className={styles.Role}>{props.detail.role}</div>
                <div className={styles.Team}>{props.detail.team}</div>
            </div>
        </div>
    );
}