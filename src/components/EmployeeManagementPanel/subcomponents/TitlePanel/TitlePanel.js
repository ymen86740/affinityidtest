import React from 'react';
import styles from './TitlePanel.module.css';
import {Select} from 'antd';
import Container from '../../../../Container';
import _ from 'lodash';

export default (props)=>{

    const state=Container.useContainer();

    return(
        <div className={styles.Container}>
            <div className={styles.Title}>
                {props.title}
            </div>
            {
                props.sort?(
                    <div className={styles.Sort}>
                        <Select style={{width:'130px'}} placeholder="Sort by" onChange={(e)=>{
                            let sortedData;
                            switch(e){
                                case "1":
                                    sortedData=_.orderBy(state.searchedEmployees,["createdDate"],["asc"]);
                                    break;
                                case "2":
                                    sortedData=_.orderBy(state.searchedEmployees,["name"],["asc"]);
                                    break;
                                default:break;
                            }
                            setTimeout(()=>{
                                state.setSearchedEmployees(sortedData);
                            },300);
                        }}>
                            <Select.Option key="1">Created Date</Select.Option>
                            <Select.Option key="2">Alphabetical</Select.Option>
                        </Select>
                    </div>
                ):<div></div>
            }
        </div>
    );
}