import React,{useContext,useState} from 'react';
import styles from './EmployeeDetail.module.css';
import Container from '../../../../Container';
import {ReactComponent as Pen} from './pen.svg';
import {ReactComponent as Delete} from './delete.svg';
import {Button,notification} from 'antd';
import {Motion,spring,presets} from 'react-motion';
import {space} from '../../../../DatabaseInterface';

export default ()=>{

    const state=Container.useContainer();

    const [loading,setLoading]=useState(false);

    let component=(
        state.employeeDetail?
        <div className={styles.Container}>
            <div className={styles.IconPanel}>
                <img className={styles.Icon} src={state.employeeDetail.profileImageUrl} alt={state.employeeDetail.name}></img>
            </div>
            <div className={styles.DescriptionPanel}>
                <div className={styles.Title}>{state.employeeDetail.name}</div>
                <div className={styles.SubTitle}>{state.employeeDetail.email}</div>

                <div className={styles.CDPanel}>
                    <div className={styles.Add} onClick={()=>{
                        state.setShowEmployeeDetail(false)
                        setTimeout(()=>{
                            state.setShowEmployeeUpdate(true);
                        },300);
                    }}>
                        <Pen className={styles.AddIcon}></Pen>
                    </div>
                    <div></div>
                    <div className={styles.Delete} onClick={()=>{
                        state.setShowConfirmPanel(true);
                    }}>
                        <Delete className={styles.DeleteIcon}></Delete>
                    </div>
                    {
                        state.showConfirmPanel?
                        <Motion defaultStyle={{opacity:0}} style={{opacity:spring(1)}}>
                            {interpolatingStyle=>(
                                <div className={styles.ConfirmPanel} style={{opacity:`${interpolatingStyle.opacity}`}}>
                                    <div>Are You Sure?</div>
                                    <div></div>
                                    <Button className={styles.Yes} size="small" loading={loading} onClick={()=>{
                                        setLoading(true);
                                        space.then(space=>space.getEntry(state.employeeDetail.id)).then(entry=>entry.delete())
                                        .then(()=>{
                                            setLoading(false);
                                            state.setShowEmployeeDetail(false);
                                            notification.success({message:state.employeeDetail.name+" Deleted",duration:3});
                                            state.setRefresh(!state.refresh);
                                            setTimeout(()=>{
                                                state.setEmployeeDetail(null);
                                            },600);
                                        })
                                        .catch(console.error)
                                    }}>YES</Button>
                                    <Button className={styles.No} size="small" onClick={()=>{
                                        state.setShowConfirmPanel(false);
                                    }}>NO</Button>
                                </div>
                            )}
                        </Motion>
                        :null
                    }
                </div>

                <div className={styles.DetailPanel}>
                    <div className={styles.DetailPanelTitle}>Role</div>
                    <div className={styles.DetailPanelTitle}>Team</div>
                    <div className={styles.DetailPanelSubTitle}>{state.employeeDetail.role}</div>
                    <div className={styles.DetailPanelSubTitle}>{state.employeeDetail.team}</div>
                </div>
                <div className={styles.DetailPanel}>
                    <div className={styles.DetailPanelTitle}>Address</div>
                    <div className={styles.DetailPanelTitle}>City</div>
                    <div className={styles.DetailPanelSubTitle}>{state.employeeDetail.address}</div>
                    <div className={styles.DetailPanelSubTitle}>{state.employeeDetail.city}</div>
                </div>

                <div className={styles.ButtonPanel}>
                    <Button type="primary" style={{width:'80%'}} size="large" onClick={()=>{
                        localStorage.setItem('selectedEmployeeId',state.employeeDetail.id);
                        notification.success({message:state.employeeDetail.name+" shared",duration:3});
                    }}>SHARE</Button>
                </div>
            </div>
        </div>
        :null
    )

    return(
        state.showEmployeeDetail?
        <Motion defaultStyle={{x:600,opacity:0}} style={{x:spring(0,presets.stiff),opacity:spring(1)}}>
            {interpolatingStyle=>(
                <div style={{transform:`translateX(${interpolatingStyle.x}px)`,opacity:`${interpolatingStyle.opacity}`}}>
                    {component}
                </div>
            )}
        </Motion>:
        <Motion defaultStyle={{x:0,opacity:1}} style={{x:spring(600),opacity:spring(0)}}>
            {interpolatingStyle=>(
                <div style={{transform:`translateX(${interpolatingStyle.x}px)`,opacity:`${interpolatingStyle.opacity}`}}>
                    {component}
                </div>
            )}
        </Motion>
    );
}