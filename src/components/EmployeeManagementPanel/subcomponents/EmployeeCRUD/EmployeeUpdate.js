import React,{useContext,useState} from 'react';
import styles from './EmployeeUpdate.module.css';
import Container from '../../../../Container';
import {ReactComponent as Close} from './close.svg';
import {Form,Select,Input,Button,notification} from 'antd';
import {Motion,spring,presets} from 'react-motion';
import {space} from '../../../../DatabaseInterface';

const UpdateForm=(props)=>{

    const state=Container.useContainer();

    const [loading,setLoading]=useState(false);

    let component=(
        <div className={styles.Container}>
            <Close className={styles.CloseIcon} onClick={()=>{
                state.setShowEmployeeUpdate(false);
            }}></Close>
            {
                <Form onSubmit={(e)=>{
                    e.preventDefault();
                    setLoading(true);
                    props.form.validateFieldsAndScroll((err,values)=>{
                        if(!err){
                            space.then(space=>space.getEntry(state.employeeDetail.id))
                            .then(entry=>{
                                entry.fields.name['en-US']=values.name;
                                entry.fields.email['en-US']=values.email;
                                entry.fields.role['en-US']=values.role;
                                entry.fields.team['en-US']=values.team;
                                if(entry.fields.address){
                                    entry.fields.address['en-US']=values.address;
                                }
                                if(entry.fields.city){
                                    entry.fields.city['en-US']=values.city;
                                }
                                return entry.update()
                            })
                            .then(entry=>{
                                setLoading(false);
                                notification.success({message:values.name+" Updated",duration:3});
                                setTimeout(()=>{
                                    state.setRefresh(!state.refresh);
                                },300);
                            })
                            .catch(x=>{
                                notification.success({message:"Error",duration:3});
                                setLoading(false);
                            })
                        }
                        else{
                            setLoading(false);
                        }
                    });
                }}>
                    <Form.Item label="Profile image">
                        {props.form.getFieldDecorator('profileImage', {
                            initialValue:state.employeeDetail?state.employeeDetail.id:'',
                        })(<Select size="large" showArrow={false} disabled={true}>
                                {
                                    state.searchedEmployees.length!==0?
                                    state.searchedEmployees.map(x=>{
                                        return(
                                            <Select.Option key={x.id} value={x.id}>
                                                <img src={x.profileImageUrl} width="30px" height="30px" alt={x.name}></img>
                                                <div className={styles.Name}>{x.name}</div>
                                            </Select.Option>
                                        )
                                    })
                                    :null
                                }
                            </Select>)}
                    </Form.Item>
                    <Form.Item label="Name">
                        {props.form.getFieldDecorator('name', {
                            initialValue:state.employeeDetail?state.employeeDetail.name:'',
                            rules: [
                            {
                                required: true,
                                message: 'Please input your name!',
                            },
                            ],
                        })(<Input size="large"></Input>)}
                    </Form.Item>
                    <Form.Item label="Email">
                        {props.form.getFieldDecorator('email', {
                            initialValue:state.employeeDetail?state.employeeDetail.email:'',
                            rules: [
                            {
                                type: 'email',
                                message: 'The input is not valid email!',
                            },
                            {
                                required: true,
                                message: 'Please input your email!',
                            },
                            ],
                        })(<Input size="large"></Input>)}
                    </Form.Item>
                    <Form.Item label="Role">
                        {props.form.getFieldDecorator('role', {
                            initialValue:state.employeeDetail?state.employeeDetail.role:'',
                            rules: [
                            {
                                required: true,
                                message: 'Please select your role!',
                            },
                            ],
                        })(<Select size="large">
                                <Select.Option key="1" value="Admin">Admin</Select.Option>
                                <Select.Option key="2" value="Employee">Employee</Select.Option>
                            </Select>)}
                    </Form.Item>
                    <Form.Item label="Team">
                        {props.form.getFieldDecorator('team', {
                            initialValue:state.employeeDetail?state.employeeDetail.team:'',
                            rules: [
                            {
                                required: true,
                                message: 'Please select your team!',
                            },
                            ],
                        })(<Select size="large">
                                <Select.Option key="1" value="Management">Management</Select.Option>
                                <Select.Option key="2" value="Creative">Creative</Select.Option>
                                <Select.Option key="1" value="Finance & Admin">Finance & Admin</Select.Option>
                            </Select>)}
                    </Form.Item>
                    <Form.Item label="Address">
                        {props.form.getFieldDecorator('address', {
                            initialValue:state.employeeDetail?state.employeeDetail.address:'',
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input your address!',
                                },
                            ],
                        })(<Input size="large"></Input>)}
                    </Form.Item>
                    <Form.Item label="City">
                        {props.form.getFieldDecorator('city', {
                            initialValue:state.employeeDetail?state.employeeDetail.city:'',
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input your city!',
                                },
                            ],
                        })(<Input size="large"></Input>)}
                    </Form.Item>
                    <Form.Item>
                        <Button size="large" loading={loading} style={{width:'100%',color:'#ffffff',backgroundColor:'green',borderColor:'green'}} htmlType="submit">UPDATE EMPLOYEE</Button>
                    </Form.Item>
                </Form>
            }
        </div>
    )

    return(
        state.showEmployeeUpdate?
        <Motion defaultStyle={{x:600,opacity:0}} style={{x:spring(0,presets.stiff),opacity:spring(1)}}>
            {interpolatingStyle=>(
                <div style={{transform:`translateX(${interpolatingStyle.x}px)`,opacity:`${interpolatingStyle.opacity}`}}>
                    {component}
                </div>
            )}
        </Motion>
        :null
    );
}

export default Form.create({name:'update'})(UpdateForm);