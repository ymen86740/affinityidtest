import React,{useEffect,useState} from 'react';
import styles from './EmployeeCreate.module.css';
import Container from '../../../../Container';
import {ReactComponent as Close} from './close.svg';
import {Form,Select,Input,Button,notification} from 'antd';
import {Motion,spring,presets} from 'react-motion';
import {space,locale} from '../../../../DatabaseInterface';

const CreateForm=(props)=>{

    const state=Container.useContainer();
    const [loading,setLoading]=useState(false);

    useEffect(()=>{
        space.then(space=>space.getAssets())
        .then(response=>{
            state.setProfileImages(response.items);
        })
        .catch(console.error)
    },[]);

    let component=(
        <div className={styles.Container}>
            <Close className={styles.CloseIcon} onClick={()=>{
                state.setShowEmployeeCreate(false);
            }}></Close>
            {
                <Form onSubmit={(e)=>{
                    e.preventDefault();
                    setLoading(true);
                    props.form.validateFieldsAndScroll((err,values)=>{
                        if(!err){
                            let data={
                                fields:{
                                    profileImage:{'en-US':{sys:{type:"Link",linkType:"Asset",id:values.profileImage}}},
                                    name:{'en-US': values.name},
                                    email:{'en-US': values.email},
                                    role:{'en-US': values.role},
                                    team:{'en-US': values.team},
                                    address:{'en-US': values.address},
                                    city:{'en-US': values.city}
                                }
                            }
                            space.then((space=>space.createEntry('employees',data)))
                            .then(entry=>{
                                setLoading(false);
                                notification.success({message:values.name+" Created",duration:3});
                                setTimeout(()=>{
                                    state.setRefresh(!state.refresh);
                                },300);
                            })
                            .catch(x=>{
                                notification.error({message:"Error",duration:3});
                                setLoading(false);
                            })
                        }
                        else{
                            setLoading(false);
                        }
                    });
                }}>
                    <Form.Item label="Profile image">
                        {props.form.getFieldDecorator('profileImage', {
                            rules: [
                            {
                                required: true,
                                message: 'Please select your profile image!',
                            },
                            ],
                        })(<Select size="large">
                                {
                                    state.profileImages.length!==0?state.profileImages.map(x=>{
                                        return(
                                            <Select.Option key={x.sys.id} value={x.sys.id}>
                                                <img src={x.fields.file[locale].url} width="30px" height="30px" alt={x.fields.title[locale]}></img>
                                                <div className={styles.Name}>{x.fields.title[locale]}</div>
                                            </Select.Option>
                                        )
                                    }):null
                                }
                            </Select>)}
                    </Form.Item>
                    <Form.Item label="Name">
                        {props.form.getFieldDecorator('name', {
                            rules: [
                            {
                                required: true,
                                message: 'Please input your name!',
                            },
                            ],
                        })(<Input size="large"></Input>)}
                    </Form.Item>
                    <Form.Item label="Email">
                        {props.form.getFieldDecorator('email', {
                            rules: [
                            {
                                type: 'email',
                                message: 'The input is not valid email!',
                            },
                            {
                                required: true,
                                message: 'Please input your email!',
                            },
                            ],
                        })(<Input size="large"></Input>)}
                    </Form.Item>
                    <Form.Item label="Role">
                        {props.form.getFieldDecorator('role', {
                            rules: [
                            {
                                required: true,
                                message: 'Please select your role!',
                            },
                            ],
                        })(<Select size="large">
                                <Select.Option key="1" value="Admin">Admin</Select.Option>
                                <Select.Option key="2" value="Employee">Employee</Select.Option>
                            </Select>)}
                    </Form.Item>
                    <Form.Item label="Team">
                        {props.form.getFieldDecorator('team', {
                            rules: [
                            {
                                required: true,
                                message: 'Please select your team!',
                            },
                            ],
                        })(<Select size="large">
                                <Select.Option key="1" value="Management">Management</Select.Option>
                                <Select.Option key="2" value="Creative">Creative</Select.Option>
                                <Select.Option key="1" value="Finance & Admin">Finance & Admin</Select.Option>
                            </Select>)}
                    </Form.Item>
                    <Form.Item label="Address">
                        {props.form.getFieldDecorator('address', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input your address!',
                                },
                            ],
                        })(<Input size="large"></Input>)}
                    </Form.Item>
                    <Form.Item label="City">
                        {props.form.getFieldDecorator('city', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input your city!',
                                },
                            ],
                        })(<Input size="large"></Input>)}
                    </Form.Item>
                    <Form.Item>
                        <Button size="large" loading={loading} style={{width:'100%',color:'#ffffff',backgroundColor:'green',borderColor:'green'}} htmlType="submit">ADD EMPLOYEE</Button>
                    </Form.Item>
                </Form>
            }
        </div>
    )

    return(
        state.showEmployeeCreate?
        <Motion defaultStyle={{x:600,opacity:0}} style={{x:spring(0,presets.stiff),opacity:spring(1)}}>
            {interpolatingStyle=>(
                <div style={{transform:`translateX(${interpolatingStyle.x}px)`,opacity:`${interpolatingStyle.opacity}`}}>
                    {component}
                </div>
            )}
        </Motion>
        :null
    );
}

export default Form.create({name:'create'})(CreateForm);