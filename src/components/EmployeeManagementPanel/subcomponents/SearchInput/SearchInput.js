import React,{useContext} from 'react';
import styles from './SearchInput.module.css';
import Container from '../../../../Container';

export default ()=>{

    const state=Container.useContainer();

    return(
        <input className={styles.SearchInput} placeholder="Search:" onKeyUp={(e)=>{
            state.setSearchInputValue(e.target.value);
            let result=state.employees.filter(x=>{
                switch(e.target.value){
                    case "":return x;
                    default:return x.name.match(new RegExp(e.target.value,"gi")) && e.target.value.match(/\S/gi)?x:null;
                }
            })
            state.setSearchedEmployees(result);
        }}></input>
    );
}