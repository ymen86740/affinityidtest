import React from 'react';
import styles from './AppLayout.module.css';

export default (props)=>{

    return(
        <div className={styles.Container}>
            <div></div>
            <div>
                {props.children}
            </div>
            <div></div>
        </div>
    );
}