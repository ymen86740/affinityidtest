import React from 'react';
import './App.css';
import AppLayout from './components/AppLayout/AppLayout';
import EmployeeManagementPanel from './components/EmployeeManagementPanel/EmployeeManagementPanel';
import Container from './Container';

export default ()=>{
  return (
    <div className="App">
      <Container.Provider>
        <AppLayout>
          <div></div>
            <EmployeeManagementPanel></EmployeeManagementPanel>
          <div></div>
        </AppLayout>
      </Container.Provider>
    </div>
  );
}
