import {useState} from 'react';
import {createContainer} from "unstated-next";

const Container=createContainer(()=>{

    const [employees,setEmployees]=useState([]);
    const [searchedEmployees,setSearchedEmployees]=useState([]);
    const [searchInputValue,setSearchInputValue]=useState('');
    const [showEmployeeDetail,setShowEmployeeDetail]=useState(false);
    const [showEmployeeUpdate,setShowEmployeeUpdate]=useState(false);
    const [showEmployeeCreate,setShowEmployeeCreate]=useState(false);
    const [employeeDetail,setEmployeeDetail]=useState(null);
    const [showConfirmPanel,setShowConfirmPanel]=useState(false);
    const [profileImages,setProfileImages]=useState([]);
    const [refresh,setRefresh]=useState(false);

    return {
        employees,setEmployees,
        searchedEmployees,setSearchedEmployees,
        searchInputValue,setSearchInputValue,
        showEmployeeDetail,setShowEmployeeDetail,
        showEmployeeUpdate,setShowEmployeeUpdate,
        showEmployeeCreate,setShowEmployeeCreate,
        employeeDetail,setEmployeeDetail,
        showConfirmPanel,setShowConfirmPanel,
        profileImages,setProfileImages,
        refresh,setRefresh
    }
});

export default Container