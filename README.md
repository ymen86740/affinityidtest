This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To run this project locally, run npm i / yarn firstly, then npm start / yarn start

This project is based on Client Side Rendering, using 'Antd Design' as the third-party UI library, 'React Motion' as the animation library, 'lodash' as the utility library, 'Contentful' as the backend database. All the employees' data is content manageable at the backend(Login in Contentful with google account: ymen86740/my831226).

Note:
1.Because we want the profile image looks beautiful, we don't want users to upload the image by themselves. In this case, we can manage all the profile images in CMS. After adding images in 'Contentful', users can view these images on 'Create New Employee'.

2.This project also adopt React Hooks API to manage state(redux pattern), see more in 'EmployeeManagementPanelStore.js' file.